const note = {
  notes: [
    {
      _id: "1", //MongoDb string..
      note: "First Note",
    },
    {
      _id: "2",
      note: "Second Note",
    },
  ],
};

const note1 = {
  note: "Third Note",
};

// class Product {
//   constructor(id, name, type) {
//     this.id = id;
//     this.name = name;
//     this.type = type;
//   }
// }

module.exports = { note, note1 };

import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { note } from "../mockdata";
import { note1 } from "../mockdata";
import { getNote } from "../../src/API";
import { postNote } from "../../src/API";

const testUrl = "localhost:4001";
const mock = new MockAdapter(axios);

describe("Note App API Tests", () => {
  afterAll(() => mock.restore()); //Hepsinden sonra yeniler.
  beforeEach(() => mock.reset()); //Her bir çalışmadan önce mocka reset atılır.

  it("Get Notes From Data Set", async () => {
    mock.onGet(testUrl).reply(200, note);
    //const notalar = JSON.parse(note);
    //console.log("Note = " + notalar.notes);

    const response = await getNote(testUrl);
    expect(response.status).toBe(200);
    expect(response.data).toStrictEqual(note);

    //console.log("Response = " + response.data[0]);
  });

  it("Post Notes From Data Set", async () => {
    //JSON data göndermek gerekiyor !!
    mock.onPost(testUrl).reply(201);

    const response = await postNote(testUrl, note1);

    expect(response.status).toBe(201);
  });
});

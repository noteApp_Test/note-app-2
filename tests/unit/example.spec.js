import { shallowMount } from "@vue/test-utils";
import NoteApp from "@/components/NoteApp.vue";

describe("NoteApp.vue", () => {
  // it("renders props.msg when passed", () => {
  //   const msg = "new message";
  //   const wrapper = shallowMount(NoteApp, {
  //     propsData: { msg },
  //   });
  //   expect(wrapper.text()).toMatch(msg);
  // });

  const wrapper = shallowMount(NoteApp, {
    data() {
      return {};
    },
  });

  it("is textBox rendered", () => {
    expect(wrapper.find("#textBox").exists()).toBeTruthy();
  });

  it("is btnAddNote rendered", () => {
    expect(wrapper.find("#btnAddNote").exists()).toBeTruthy();
    expect(wrapper.find("#btnAddNote").text()).toBe("Add Note");
  });

  it("is lstNote rendered", () => {
    expect(wrapper.find("#lstNote").exists()).toBeTruthy();
  });
});

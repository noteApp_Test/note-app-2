const axios = require("axios");

const getNote = async (testUrl) => {
  return axios.get(testUrl);
};

const postNote = async (testUrl, note1) => await axios.post(testUrl, note1);

module.exports = { getNote, postNote };

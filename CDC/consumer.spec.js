const { Pact } = require("@pact-foundation/pact");
const chai = require("chai");
const assert = require("assert");
const expect = chai.expect;
const { note, note1 } = require("../tests/mockdata");
const { getNote, postNote } = require("../src/API");
const myPort = 8800;
const testUrl = "http://localhost:" + myPort;

// (1) Import the pact library and matching methods
//const { Matchers } = require("@pact-foundation/pact");
//const { like, regex } = Matchers;

describe("Notes API test", () => {
  // (2) Configure our Pact library
  const provider = new Pact({
    port: myPort,
    consumer: "get-note",
    provider: "post-note",
    pactfileWriteMode: "update",
  });

  // (3) Setup Pact lifecycle hooks
  before(() => provider.setup());
  after(() => provider.finalize());

  describe("When a requst is sent to get note API", () => {
    describe("When a GET requst is sent to post note API", () => {
      before(() => {
        return provider.addInteraction({
          uponReceiving: "Get Note",
          withRequest: {
            path: "/",
            method: "GET",
          },
          willRespondWith: {
            status: 200,
            body: { note },
          },
        });
      });
      it("Will recive the list of todo", async () => {
        getNote(testUrl)
          .then((r) => {
            assert.ok(r.status === 200);
          })
          .catch((error) => {
            throw new Error(error.message || error);
          });
      });
    });
  });
});
